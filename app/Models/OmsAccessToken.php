<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class OmsAccessToken extends Model 
{
    

    protected $table = 'oms_access_token';
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'userId', 'token',
    ];

    

}
