<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use  App\Models\OmsUser;

class AuthController extends Controller
{
    /**
     * Store a new user.
     *
     * @param  Request  $request
     * @return Response
     */
    public function register(Request $request)
    {
        //validate incoming request 
        $this->validate($request, [
            'name' => 'required|string',
            'email' => 'required|email|unique:oms_users',
            'password' => 'required|confirmed',
            'firstname' => 'required',
            'title' => 'required',
            'realm'=>'required',
        ]);

        try {

            $user = new OmsUser;
            $user->username = $request->input('name');
            $user->email = $request->input('email');
            $plainPassword = $request->input('password');
            $user->firstname = $request->input('firstname');
            $user->title = $request->input('title');
            $user->realm = $request->input('realm');
            $user->last_password_changed = date('Y-m-d H:i:s',time());
            $user->password = app('hash')->make($plainPassword);
            // $user->password = md5($plainPassword);

            $user->save();

            //return successful response
            return response()->json(['user' => $user, 'message' => 'CREATED'], 201);

        } catch (\Exception $e) {
            //return error message
            return response()->json(['message' => 'User Registration Failed!','error'=>$e->getMessage()], 409);
        }

    }
    public function login(Request $request)
    {
          //validate incoming request 
        $this->validate($request, [
            'email' => 'required|string',
            'password' => 'required|string',
        ]);

        $credentials = $request->only(['email', 'password']);
// print_r($credentials);
        if (! $token = Auth::attempt($credentials)) {
            return response()->json(['message' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }
}